import 'package:flutter/material.dart';

/// App icon avatar
Widget appAvatar({double avatarRadius}) {
  return CircleAvatar(
      radius: avatarRadius + 5,
      backgroundColor: Colors.black26,
      child: CircleAvatar(
        radius: avatarRadius,
        backgroundImage: AssetImage('assets/app_icon.jpg'),
      ));
}
