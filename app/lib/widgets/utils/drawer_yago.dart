import 'package:flutter/material.dart';
import '../../screens/settings.dart';
import '../../screens/yesno.dart';
import '../../utils/strings.dart' as constants;
import 'logo.dart';

/// App drawer
class DrawerYago extends StatefulWidget {
  @override
  _DrawerYagoState createState() => _DrawerYagoState();
}

class _DrawerYagoState extends State<DrawerYago> {
  void initState() {
    // setState(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Column(
      children: [
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: appAvatar(avatarRadius: 61),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/drawer_background.png'),
                      fit: BoxFit.fill),
                ),
              ),
              ScreenTile(
                  Icons.local_activity,
                  constants.drawerYesNoTitle,
                  constants.drawerYesNoSubtitle,
                  MaterialPageRoute(builder: (context) => YesNoScreen())),
              Divider(),
              ScreenTile(Icons.keyboard, constants.drawerABCTitle,
                  constants.drawerABCSubtitle, null),
              //MaterialPageRoute(
              //    builder: (context) =>
              //        YesNoScreen(drawerTimeout: _numShowed))),
              Divider()
            ],
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(bottom: 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Divider(),
                ScreenTile(
                    Icons.settings,
                    constants.drawerSettingsTitle,
                    constants.drawerSettingsSubtitle,
                    MaterialPageRoute(builder: (context) => SettingsScreen())),
              ],
            ),
          ),
        )
      ],
    ));
  }
}

/// Different screens in the drawer
class ScreenTile extends StatelessWidget {
  final IconData _iconTile;
  final String _textTile, _subtextTile;
  final MaterialPageRoute _mpr;
  //final Widget _onTap;

  /// constructor
  ScreenTile(this._iconTile, this._textTile, this._subtextTile, this._mpr);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(_iconTile, size: 35, color: Colors.purple),
      title:
          Text(_textTile, style: TextStyle(fontSize: 16, color: Colors.black)),
      subtitle: Text(_subtextTile),
      onTap: () => Navigator.push(context, _mpr),
      trailing: Icon(Icons.keyboard_arrow_right, size: 25),
    );
  }
}
