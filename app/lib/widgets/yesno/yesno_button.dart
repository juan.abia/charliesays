import 'package:flutter/material.dart';

// CONSTANTS
///
final double minButtonHeight = 70;

///
final double maxButtonHeight = 130;

///
final double minButtonWidth = 90;

///
final double maxButtonWidth = 150;

/// Main page button!
Widget yesNoButton(
    {double buttonHeight,
    double button2Height,
    double buttonWidth,
    String buttonText,
    Color buttonDarkColor,
    Color buttonLightColor,
    Color borderColor,
    Duration duration,
    void onEnd()}) {
  var _radius = 18.0;
  return Stack(
    alignment: AlignmentDirectional.bottomCenter,
    children: [
      AnimatedContainer(
        height: button2Height,
        width: buttonWidth,
        decoration: BoxDecoration(
          color: buttonLightColor,
          borderRadius: bordersButton(_radius),
        ),
        /* Animation */
        duration: duration,
        curve: Curves.easeOut,
      ),
      AnimatedContainer(
        height: buttonHeight,
        width: buttonWidth,
        decoration: BoxDecoration(
            color: buttonDarkColor,
            borderRadius: bordersButton(_radius),
            border: Border.all(color: borderColor, width: 5)),
        /* Animation */
        duration: duration,
        curve: Curves.easeOut,
        onEnd: onEnd,
      ),
      Text(
        buttonText,
        style: TextStyle(
            color: Colors.white, fontSize: 50, fontWeight: FontWeight.w600),
      ),
      Container(
        height: maxButtonHeight,
        width: maxButtonWidth,
        color: Colors.transparent,
      )
    ],
  );
}

/// Button borders
BorderRadius bordersButton(double _radius) {
  return BorderRadius.only(
      topLeft: Radius.circular(_radius), topRight: Radius.circular(_radius));
}
