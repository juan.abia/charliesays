import 'package:flutter/material.dart';

/// Different settings
class SettingsTile extends StatelessWidget {
  final IconData _iconTile;
  final String _textTile, _subtextTile;
  //final Widget _onTap;

  /// constructor
  SettingsTile(this._iconTile, this._textTile, this._subtextTile);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Icon(_iconTile, size: 35, color: Colors.purple),
        title: Text(_textTile,
            style: TextStyle(fontSize: 16, color: Colors.black)),
        subtitle: Text(_subtextTile));
  }
}
