import 'package:flutter/material.dart';
import '../../utils/strings.dart' as constants;

/// About dialog
class InfoDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Icon(Icons.info_outline, size: 35, color: Colors.purple),
        title: Text(constants.settingsInfoTitle,
            style: TextStyle(fontSize: 16, color: Colors.black)),
        subtitle: Text(constants.settingsInfoSubtitle),
        onTap: () {
          showAboutDialog(
              context: context,
              applicationName: constants.appName,
              applicationIcon: CircleAvatar(
                  backgroundImage: AssetImage('assets/app_icon.jpg')),
              applicationVersion: constants.appVersion,
              applicationLegalese: constants.appCompany,
              children: <Widget>[
                SizedBox(height: 20),
                Text(constants.appDescription,
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        color: Colors.grey[800],
                        fontSize: 18))
              ]);
        });
  }
}
