import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../utils/parameters.dart';

/// Slider to manage the yes/no timeout
class SliderYesno extends StatefulWidget {
  @override
  _SliderYesnoState createState() => _SliderYesnoState();
}

class _SliderYesnoState extends State<SliderYesno> {
  /* Padding */
  final double _shortPadding = 0.0;
  final double _longPadding = 20.0;

  /* Shared prefs */
  SharedPreferences prefs;
  double _sliderYesNoValue = yesnoDefaultTimeout.toDouble();

  Future<void> _initSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    var aux = await prefs.getInt("yesnoTimeout");
    if (aux != null) {
      _sliderYesNoValue = aux.toDouble();
    } else {
      _sliderYesNoValue = yesnoDefaultTimeout.toDouble();
    }
    print("slider: $_sliderYesNoValue");
    setState(() {});
  }

  Future<void> _updateSharedPrefs(String key, int value) async {
    prefs.setInt(key, value);
  }

  void _handleSliderChange(var value) {
    setState(() {
      _sliderYesNoValue = value;
    });
    if (_sliderYesNoValue != null) {
      _updateSharedPrefs("yesnoTimeout", _sliderYesNoValue.round());
    }
  }

  /* INIT */
  @override
  void initState() {
    _initSharedPrefs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SliderLabel(
              '${yesnoMinTimeout.toString()} s', _longPadding, _shortPadding),
          Expanded(
            child: Slider(
              /* Values */
              value: _sliderYesNoValue,
              min: yesnoMinTimeout.toDouble(),
              max: yesnoMaxTimeout.toDouble(),
              divisions: yesnoStepsTimeout,
              /* Colors */
              activeColor: Colors.purple,
              inactiveColor: Colors.grey[400],
              label: _sliderYesNoValue != null
                  ? _sliderYesNoValue.round().toString()
                  : "",
              /* Actions */
              onChanged: _handleSliderChange,
            ),
          ),
          SliderLabel(
              '${yesnoMaxTimeout.toString()} s', _shortPadding, _longPadding)
        ]);
  }
}

///
class SliderLabel extends StatelessWidget {
  final String _textLabel;
  final double _leftPadding;
  final double _rightPadding;

  /// Constructor
  SliderLabel(this._textLabel, this._leftPadding, this._rightPadding);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: _leftPadding, right: _rightPadding),
      child: Text(_textLabel,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500)),
    );
  }
}
