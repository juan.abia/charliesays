//Packages
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
//Relative imports
import 'screens/yesno.dart';

/// CharlieSays!
void main() {
  runApp(CharlieSays());
}

/// App Root
class CharlieSays extends StatelessWidget {
  /* Default values */
  final _defaultYesNoTimeout = 10;

  /* Shared Prefs */
  Future<void> _initSharedPrefs() async {
    var prefs = await SharedPreferences.getInstance();
    try {
      var aux = await prefs.getInt("yesnoTimeout");
      if (aux == null) {
        prefs.setInt("yesnoTimeout", _defaultYesNoTimeout);
      }
    } on Exception {
      prefs.setInt("yesnoTimeout", _defaultYesNoTimeout);
    }
  }

  @override
  Widget build(BuildContext context) {
    // only portrait mode!
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    _initSharedPrefs();

    return MaterialApp(
      title: "Charlie Says",
      theme: ThemeData(primaryColor: Colors.purple),
      debugShowCheckedModeBanner: false,
      home: YesNoScreen(),
      /*
        initialRoute: '/',
        routes: {
          '/': (context) => YesNoScreen(),
          '/abc': (context) => ABCScreen(),
        }
      */
    );
  }
}
