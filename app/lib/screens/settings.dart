import 'package:flutter/material.dart';
import '../utils/strings.dart' as constants;
import '../widgets/settings/about.dart';
import '../widgets/settings/settings_list.dart';
import '../widgets/settings/yesno_slider.dart';
import '../widgets/utils/logo.dart';

/// Settings and general info screen
class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(constants.appName),
      ),
      body: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: appAvatar(avatarRadius: 80),
          ),
          SettingsTile(Icons.settings, constants.settingsYesNoTitle,
              constants.settingsYesNoSubtitle),
          SliderYesno(),
          Divider(),
          InfoDialog(),
        ],
      ),
    );
  }
}
