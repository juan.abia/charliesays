//Packages
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
// Relative imports (abcd)
import '../api/tts.dart';
import '../styles/yesno.dart';
import '../utils/parameters.dart';
import '../utils/strings.dart' as constants;
import '../widgets/utils/drawer_yago.dart';
import '../widgets/utils/logo.dart';
import '../widgets/yesno/yesno_button.dart';

/// Yes/No screen
class YesNoScreen extends StatefulWidget {
  @override
  _YesNoScreenState createState() => _YesNoScreenState();
}

class _YesNoScreenState extends State<YesNoScreen>
    with TickerProviderStateMixin {
  /* Single ticker needed to build the animations */

  /* Timers */
  int _yesnoTimeout; // s

  /* Timers */
  final Duration _durationButtonSize = Duration(milliseconds: timeoutSize);

  // BUTTONS
  /* Dimensions */
  double _heightYes, _height2Yes, _widthYes;
  double _heightNo, _height2No, _widthNo;
  /* Colors */
  bool _whichButton = false;
  Color _borderYesColor = Colors.transparent;
  Color _borderNoColor = Colors.transparent;

  /* ANIMATIONS */
  /* Fillin' animation */
  AnimationController _fillAnimationController;
  Animation _fillAnimation;
  Animation _fillCurve;
  /* Borders animation */
  AnimationController _borderAnimationController;
  Animation<Color> _borderAnimation;
  /* MediaQuery */
  double _mediaQueryHeight;

  /* Shared prefs */
  Future<void> getSharedPrefs() async {
    var prefs = await SharedPreferences.getInstance();
    var aux = await prefs.getInt("yesnoTimeout");
    if (aux == null) {
      _yesnoTimeout = yesnoDefaultTimeout;
    } else if (aux != _yesnoTimeout) {
      _yesnoTimeout = aux;
    }
    print('yes/no: $_yesnoTimeout');
  }

  /* Get prefs and start animation */
  void _onceReadyGoForward() {
    /* Animations */
    print('animation: $_yesnoTimeout');
    _fillAnimationController = AnimationController(
        vsync: this, duration: Duration(seconds: _yesnoTimeout));
    _fillCurve =
        CurvedAnimation(parent: _fillAnimationController, curve: Curves.linear);
    _fillAnimation =
        Tween<double>(begin: maxButtonHeight, end: 0.0).animate(_fillCurve)
          ..addListener(() {
            setState(() {
              _handleFill();
              /* For linter purposes */
              null;
            });
          });

    _borderAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: timeoutBorder));
    _borderAnimation = ColorTween(begin: borderON, end: borderON)
        .animate(_borderAnimationController)
          ..addListener(() {
            setState(() {
              _handleBorder();
              null;
            });
          });

    /* Go on kiddo */
    _fillAnimationController.forward();
  }

  /* Handle drawer change */
  void _onDrawerChanged(bool isOpen) {
    if (!isOpen) {
      getSharedPrefs().then((result) {
        /* Change time and reset animation */
        _fillAnimationController.duration = Duration(seconds: _yesnoTimeout);
        _onEndAnimatedContainer();
      });
    }
  }

  /* TTS: TextToSpeech */
  void _handleTap() {
    /* TTS */
    _whichButton ? speakTts(constants.noButton) : speakTts(constants.yesButton);
    /* Animations */
    _borderAnimationController.reset();
    _borderAnimationController.forward();
  }

  /* ANIMATIONs */
  /* Whenever the unfillin' action is completed... */
  void _handleFill() {
    _whichButton
        ? _heightNo = _fillAnimation.value
        : _heightYes = _fillAnimation.value;

    if (_fillAnimationController.isCompleted) {
      if (_whichButton) {
        /* YES */
        _heightYes = _height2Yes = maxButtonHeight;
        _widthYes = maxButtonWidth;
        _heightNo = _height2No = minButtonHeight;
        _widthNo = minButtonWidth;
      } else {
        /* NO */
        _heightYes = _height2Yes = minButtonHeight;
        _widthYes = minButtonWidth;
        _heightNo = _height2No = maxButtonHeight;
        _widthNo = maxButtonWidth;
      }
      _whichButton = !_whichButton;
    }
  }

  /* Borders animation completed */
  void _handleBorder() {
    _whichButton
        ? _borderNoColor = _borderAnimation.value
        : _borderYesColor = _borderAnimation.value;

    if (_borderAnimation.isCompleted) {
      _borderYesColor = _borderNoColor = borderOFF;
    }
  }

  /* Reboot animations */
  void _onEndAnimatedContainer() {
    _fillAnimationController.reset();
    _fillAnimationController.forward();
  }

  /* SYSTEM */
  @override
  // dispose: called when the tree is unmounted
  void dispose() {
    _fillAnimationController.dispose();
    _borderAnimationController.dispose();
    super.dispose();
  }

  /* INIT */
  void initState() {
    /* Buttons */
    _heightYes = _height2Yes = maxButtonHeight;
    _widthYes = maxButtonWidth;
    _heightNo = _height2No = minButtonHeight;
    _widthNo = minButtonWidth;

    /* Get Shared prefs */
    getSharedPrefs().then((result) {
      _onceReadyGoForward();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // responsive feature
    _mediaQueryHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text(constants.appName),
        ),
        /* Drawer */
        drawer: DrawerYago(),
        onDrawerChanged: _onDrawerChanged,
        /* Special feature: tap anywhere! */
        body: InkWell(
          onTap: _handleTap,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Spacer(flex: 2),
                Flexible(
                    fit: FlexFit.tight,
                    flex: 5,
                    child: appAvatar(avatarRadius: _mediaQueryHeight / 6.5)),
                //Spacer(flex: 1),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 4,
                        child: yesNoButton(
                            buttonHeight: _heightYes,
                            button2Height: _height2Yes,
                            buttonWidth: _widthYes,
                            buttonText: constants.yesButton,
                            buttonDarkColor: colorYesDark,
                            buttonLightColor: colorYesLight,
                            borderColor: _borderYesColor,
                            duration: _durationButtonSize,
                            onEnd: _onEndAnimatedContainer),
                      ),
                      Spacer(flex: 1),
                      Flexible(
                        flex: 4,
                        child: yesNoButton(
                            buttonHeight: _heightNo,
                            button2Height: _height2No,
                            buttonWidth: _widthNo,
                            buttonText: constants.noButton,
                            buttonDarkColor: colorNoDark,
                            buttonLightColor: colorNoLight,
                            borderColor: _borderNoColor,
                            duration: _durationButtonSize),
                      )
                    ],
                  ),
                ),
                Spacer(flex: 2)
              ],
            ),
          ),
        ));
  }
}
