import 'package:flutter_tts/flutter_tts.dart';

FlutterTts _flutterTts = FlutterTts();

/// Tts current state
enum TtsState {
  /// Sound is being played
  playing,

  /// https://www.youtube.com/watch?v=6zvIxD4FUTA&t=854s
  stopped,

  /// Meta-state = problems
  cheddar
}

///
TtsState ttsState = TtsState.stopped;

/// Words don't come easy
Future speakTts(String _what) async {
  var result = await _flutterTts.speak(_what);
  if (result == 1) () => ttsState = TtsState.playing;
}
