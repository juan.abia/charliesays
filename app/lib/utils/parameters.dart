/// Default YesNo Time
const int yesnoDefaultTimeout = 10;

/// Min YesNo Time
const int yesnoMaxTimeout = 30;

/// Min YesNo Time
const int yesnoMinTimeout = 1;

/// Min YesNo Time
const int yesnoStepsTimeout = 8;

/// Size change animation between yes/no buttons
const int timeoutSize = 900; //ms

/// Time of the borders animation
const int timeoutBorder = 1000; //ms
