import 'dart:async';

/// Initiate timeout
void startTimeout({Timer timer, int timeout = 10, void handleTmr()}) {
  timer = Timer.periodic(Duration(seconds: timeout), (timer) {
    handleTmr();
  });
}
