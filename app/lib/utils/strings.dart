/// APP NAME
const String appName = 'CharlieSays';

/// APP VERSION
const String appVersion = '1.1.0';

/// APP DESCRIPTION
const String appDescription =
    'https://gitlab.com/juan.abia/charliesays/-/wikis/home';

/// APP COMPANY
const String appCompany = 'IEEE UVigo ©';

// drawer
///
const String drawerYesNoTitle = 'Si/No';

///
const String drawerYesNoSubtitle = 'O "Si" o "No"';

///
const String drawerABCTitle = 'ABC';

///
const String drawerABCSubtitle = 'Un teclado simple';

///
const String drawerSettingsTitle = 'Ajustes & Info';

///
const String drawerSettingsSubtitle = 'Cambiar tiempos, Gitlab, ...';

// yes/no screen
///
const String yesButton = 'Si';

///
const String noButton = 'No';

// settings & info
///
const String settingsYesNoTitle = 'Tiempo Si/No';

///
const String settingsYesNoSubtitle =
    'La idea es empezar con tiempos altos, e ir reduciéndolos...';

///
const String settingsInfoTitle = 'Más info';

///
const String settingsInfoSubtitle = 'Desarrolladores, Gitlab page, ...';
