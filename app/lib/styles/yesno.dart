import 'package:flutter/material.dart';

/* BUTTONS */
///
final Color colorYesLight = Colors.black12;

///
final Color colorYesDark = Colors.green;

///
final Color colorNoLight = Colors.black12;

///
final Color colorNoDark = Colors.red;

///
final Color borderOFF = Colors.transparent;

///
final Color borderON = Colors.black;
