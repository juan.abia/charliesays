import matplotlib.pyplot as plt
import csv


def myround(n, base=5):
    return int(base * round(float(n)/base))


def tlimit(n, signalv, y, limit):
    a = 0
    for i in range(len(signalv)):
        if signalv[i] > 0:
            a += 1
            if a == n:
                for j in range(i, len(y)):
                    if y[j] >= limit:
                        return (j - i) / 10


# Reading csv file
results = []
with open("test.csv") as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        results.append(row)

# Creating plotting vectors
x, y, signal, limitv, limit = [], [], [], [], 600
for i in range(len(results)):
    if results[i] == ["a"]:
        signal.append(limit)
    elif not results[i]:
        pass
    else:
        signal.append(0)
        x.append(float(results[i][0])/10)
        y.append(int(results[i][1]))
limitv = [limit] * len(x)
signal = signal[0:len(x)]

# plotting parameters
xdiv = 6
ydiv = 4
xlim = x[-1]
ylim = max(y)
xu = myround((xlim + 0.5*xlim)/xdiv, 5)
yu = myround((ylim + 0.5*ylim)/ydiv, 25)
if xu == 0:
    xu = 5

# Time to get to the threshold
print(tlimit(1, signal, y, limit))

# Plot
plt.plot(x, y)
plt.plot(x, signal)
plt.plot(x, limitv)
plt.xlabel("Tiempo")
plt.ylabel("Fuerza")
plt.xticks([a*xu for a in range(xdiv)])
plt.yticks([a*yu for a in range(ydiv)])
plt.show()
