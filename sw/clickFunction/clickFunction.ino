#include <Mouse.h>


int v = 0;
int threshold = 600;
int min_pulso = 2000;
int tik, tok = 0;
int t = 0;
char a = "";
void setup() {
  pinMode(A0, INPUT);
  Serial.begin(9600);
  Mouse.begin();
  delay(4000);
}

void loop() {
  v = analogRead(A0);
  tik = millis();
  while (v > threshold){
    tok = millis();

    v = analogRead(A0);
    Serial.println(v);
    Serial.print(",");
    Serial.println(threshold);
    delay(100);
    
    if ((tok - tik) > min_pulso){
      Mouse.click();
      v = 0;
      }
    } 
  
  // Uncomment to plot values
  //Serial.print(t);
  //Serial.print(",");
  
  Serial.println(v);
  
  // Uncomment to view values on serial plotter
  Serial.print(",");
  Serial.println(threshold);
  
  delay(100);
  t++;

  // Signal "a"
  if (Serial.available() > 0) {
    a = Serial.read();
    Serial.println(a);
  }
}
