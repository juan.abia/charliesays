## Charlie Says

This project is aimed to allow communication with 
my cousin Carlos, who has brain paralysis.

<p align="center">
  <img src="doc/generic_figures/micro.jpg" width="320" title="repo icon">
</p>

- sw: @juan.abia & @scorrea
- hw: @Fer6Flores
- app: @D.Furry & @A_Granja & @_arisu & @mateomotriz

*IEEE UVigo Student Branch ©* 
